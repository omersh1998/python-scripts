import xml.etree.ElementTree as ET
import configparser

# configuration

config = configparser.ConfigParser()
config.read('./config/config.ini')

# variables from config

FILENAME = config['DEFAULT']['FILE_NAME']
xmlEntries = []

# xml tree initiation

tree = ET.parse(FILENAME)
root = tree.getroot()

# root = ET.fromstring("url")

for entry in root:
    temp = []
    for child in entry:
        tup = (child.tag, child.text)
        if len(child.attrib) != 0:
            print(child.attrib['unixTime'])
        temp.append(tup)
    xmlEntries.append(temp)
print(xmlEntries[0])
